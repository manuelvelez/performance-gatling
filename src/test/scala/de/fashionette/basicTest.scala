import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._

// Mandatory, you must import Flood libraries

class TestPlan extends Simulation {
  // Optional, Flood IO will pass in threads, rampup and duration properties from UI
  val threads   = Integer.getInteger("threads",  100)
  val rampup    = java.lang.Long.getLong("rampup", 30L)
  val duration  = java.lang.Long.getLong("duration", 60L)

  // Mandatory, you must use httpConfigFlood
  val httpProtocol = http
    .baseURL("https://flooded.io/")
    .acceptHeader("text/html,application/xhtml+xml,application/xml;")
    .acceptEncodingHeader("gzip, deflate")

  val myScenario = scenario("Scenario Name")
    .during(duration seconds) {
      exec(http("home_page")
        .get("/"))
        .pause(1000 milliseconds, 5000 milliseconds)
    }

  setUp(myScenario.inject(rampUsers(threads) over (rampup seconds))).protocols(httpProtocol)
}
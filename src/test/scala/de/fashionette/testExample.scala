package de.fashionette

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

//import flood._

/**
  * Created by mave on 17/01/2017.
  */
class testExample extends Simulation{

  val httpProtocol = http
    .baseURL("http://www.fashionette.de")
    .inferHtmlResources()
    .acceptHeader("image/png,image/*;q=0.8,*/*;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:46.0) Gecko/20100101 Firefox/46.0")

  val scn01 = scenario("armani-jeans-pvc-logo-shopper-black-1")
    .exec(http("HOME").get("/"))
    .exec(http("product").get("/armani-jeans-pvc-logo-shopper-black-1"))

  val scn02 = scenario("guess-cate-satchel-black")
    .exec(http("HOME").get("/"))
    .exec(http("product").get("/guess-cate-satchel-black"))

  val scn03 = scenario("coccinelle-borsa-pelle-nabuk-brown")
    .exec(http("HOME").get("/"))
    .exec(http("product").get("/coccinelle-borsa-pelle-nabuk-brown"))

  val scn04 = scenario("mcm-stark-backpack-medium-black")
    .exec(http("HOME").get("/"))
    .exec(http("product").get("/mcm-stark-backpack-medium-black"))

  val scn05 = scenario("michael-kors-grant-md-messenger-black")
    .exec(http("HOME").get("/"))
    .exec(http("product").get("/michael-kors-grant-md-messenger-black"))

  val totalScen = scn01.exec(scn02).exec(scn03).exec(scn04).exec(scn05)

  setUp(totalScen.inject(constantUsersPerSec(1) during(2 minutes)))
    .protocols(httpProtocol)
}

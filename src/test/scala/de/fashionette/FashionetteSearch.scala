package de.fashionette

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class FashionetteSearch extends Simulation {

	val httpProtocol = http
		.baseURL("http://www.fashionette.de")
		.inferHtmlResources()
		.acceptHeader("image/png,image/*;q=0.8,*/*;q=0.5")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-US,en;q=0.5")
		.userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:46.0) Gecko/20100101 Firefox/46.0")

	val headers_0 = Map("Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")

	val headers_1 = Map("Accept" -> "*/*")

	val headers_7 = Map(
		"Accept" -> "application/json",
		"Origin" -> "http://www.fashionette.de")

	val headers_58 = Map("Accept" -> "text/css,*/*;q=0.1")

	val headers_75 = Map(
		"Accept" -> "application/font-woff2;q=1.0,application/font-woff;q=0.9,*/*;q=0.8",
		"Accept-Encoding" -> "identity")

    val uri1 = "http://ybux2ksgw5-dsn.algolia.net/1/indexes/*/queries"
    val uri2 = "http://dev.visualwebsiteoptimizer.com"

	val scn = scenario("FashionetteSearch")
		.exec(http("request_0")
			.get("/")
			.headers(headers_0)
			.resources(http("request_1")
			.get(uri2 + "/j.php?a=210073&u=http%3A%2F%2Fwww.fashionette.de%2F&r=0.6543174282299541")
			.headers(headers_1),
            http("request_2")
			.get(uri2 + "/v.gif?a=210073&d=fashionette.de&u=E25948ABD0556B756EDCC911F65EE0E0&h=d21367168b97c0abe53b178547b83739&t=false&r=0.7959066473459212")))
		.pause(14)
		.exec(http("request_3")
			.get("/gucci")
			.headers(headers_0)
			.resources(http("request_4")
			.get(uri2 + "/j.php?a=210073&u=http%3A%2F%2Fwww.fashionette.de%2Fgucci&r=0.23638032739863657")
			.headers(headers_1),
            http("request_5")
			.get("/media/catalog/category/gucci_de_720x162.jpg"),
            http("request_6")
			.get(uri2 + "/v.gif?a=210073&d=fashionette.de&u=E25948ABD0556B756EDCC911F65EE0E0&h=d21367168b97c0abe53b178547b83739&t=false&r=0.37635825803963274"),
            http("request_7")
			.post(uri1 + "?x-algolia-agent=Algolia%20for%20vanilla%20JavaScript%20(lite)%203.18.0%3Binstantsearch.js%201.8.0%3BMagento%20integration%20(1.7.0)&x-algolia-application-id=YBUX2KSGW5&x-algolia-api-key=YTRiN2RlMzI3NWExN2Y5NTc5N2ZiNjQ5ZjFjZDk1MWVlNGU0YWY1M2Q3ZDE3Y2E4MmI2ZWIzYWI3YTk4MGI0YXRhZ0ZpbHRlcnM9")
			.headers(headers_7)
			.formParam("""{"requests":[{"indexName":"live_de_products_merchandised","params":"query""", "")
			.formParam("hitsPerPage", "48")
			.formParam("maxValuesPerFacet", "Infinity")
			.formParam("page", "0")
			.formParam("facets", """["listing_category","size","clothes_size","glass_size","designer_channel","brand_id","is_new","quickcat_sale","price.EUR.default","color","deal_price.EUR.default","shoes_size","length","listing_category","categories.level0","categories.level1","categories.level2"]""")
			.formParam("tagFilters", "")
			.formParam("facetFilters", """["listing_category:141",["categories.level1:All /// Produktkategorien"]]""")
			.formParam("numericFilters", """["visibility_catalog=1"]"},{"indexName":"live_de_products_merchandised","params":"query=""")
			.formParam("hitsPerPage", "1")
			.formParam("maxValuesPerFacet", "Infinity")
			.formParam("page", "0")
			.formParam("attributesToRetrieve", "[]")
			.formParam("attributesToHighlight", "[]")
			.formParam("attributesToSnippet", "[]")
			.formParam("tagFilters", "")
			.formParam("facets", """["categories.level0","categories.level1"]""")
			.formParam("numericFilters", """["visibility_catalog=1"]""")
			.formParam("facetFilters", """["listing_category:141"]"},{"indexName":"live_de_products_merchandised","params":"query=""")
			.formParam("hitsPerPage", "1")
			.formParam("maxValuesPerFacet", "Infinity")
			.formParam("page", "0")
			.formParam("attributesToRetrieve", "[]")
			.formParam("attributesToHighlight", "[]")
			.formParam("attributesToSnippet", "[]")
			.formParam("tagFilters", "")
			.formParam("facets", """["categories.level2"]""")
			.formParam("numericFilters", """["visibility_catalog=1"]""")
			.formParam("facetFilters", """["listing_category:141"]"}]}"""),
            http("request_8")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74192-02.jpg"),
            http("request_9")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76854-02.jpg"),
            http("request_10")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-73415_1.jpg"),
            http("request_11")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76830-02.jpg"),
            http("request_12")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76588_2.jpg"),
            http("request_13")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76106_2.jpg"),
            http("request_14")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74438-02.jpg"),
            http("request_15")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-67565_1.jpeg"),
            http("request_16")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-69147-2.jpg"),
            http("request_17")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74182_2_.jpg"),
            http("request_18")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74121_74117-04_3_.jpg"),
            http("request_19")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74383_2_.jpg"),
            http("request_20")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74102_2_.jpg"),
            http("request_21")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74186-02.jpg"),
            http("request_22")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74419-3.jpg"),
            http("request_23")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74065-02.jpg"),
            http("request_24")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76205-2.jpg"),
            http("request_25")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76220_1.jpg"),
            http("request_26")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-71024_71031_4_3_.jpg"),
            http("request_27")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74276-02.jpg"),
            http("request_28")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-61738-61729_2_.jpg"),
            http("request_29")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-61348-02.jpg"),
            http("request_30")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74070_2_.jpg"),
            http("request_31")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-75106-1.jpg"),
            http("request_32")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci_75743_3.jpg"),
            http("request_33")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76130_2.jpg"),
            http("request_34")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76237_2.jpg"),
            http("request_35")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76114_2.jpg"),
            http("request_36")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76827-1.jpg"),
            http("request_37")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-75740-2.jpg"),
            http("request_38")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74157_74159-02_1_.jpg"),
            http("request_39")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76236_1.jpg"),
            http("request_40")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-69154-2.jpg"),
            http("request_41")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74148_1_.jpg"),
            http("request_42")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-72575-2.jpg"),
            http("request_43")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76022-2.jpg"),
            http("request_44")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-74100_74091-04_3_.jpg"),
            http("request_45")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76269_1.jpg"),
            http("request_46")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci_65995_1_2.jpg"),
            http("request_47")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76187_2.jpg"),
            http("request_48")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76226_2.jpg"),
            http("request_49")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-67516_2.jpg"),
            http("request_50")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76858-1.jpg"),
            http("request_51")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-72355-02.jpg"),
            http("request_52")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-61798_61797_2_.jpg"),
            http("request_53")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-67795_1.jpg"),
            http("request_54")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci_52282_1.jpg"),
            http("request_55")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76127_2.jpg"),
            http("request_56")
			.get("/media/catalog/product/cache/1/image/543x543/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76114_3.jpg"),
            http("request_57")
			.get("/gucci-lion-leather-crossbody-bag-black")
			.headers(headers_0),
            http("request_58")
			.get("/media/css/6e8b4ecaa8823309c8873e22eed54be1-1-1484041892.css")
			.headers(headers_58),
            http("request_59")
			.get("/js/merged/cbf03ed0237a052fe8eec6ef3cae95fc-1484039661.js")
			.headers(headers_1),
            http("request_60")
			.get("/media/css/466e643e579a728bbcc46258aa18c93f-1-1484041888.css")
			.headers(headers_58),
            http("request_61")
			.get("/js/merged/35c3eab826e454705651eaabac539ec5-1484041900.js")
			.headers(headers_1),
            http("request_62")
			.get("/skin/frontend/fashionette/default/images/ec-card.png"),
            http("request_63")
			.get("/skin/frontend/fashionette/default/images/invoice.jpg"),
            http("request_64")
			.get("/skin/frontend/fashionette/default/images/installment.jpg"),
            http("request_65")
			.get("/skin/frontend/fashionette/default/images/return.jpg"),
            http("request_66")
			.get("/skin/frontend/fashionette/default/images/dhl.jpg"),
            http("request_67")
			.get("/skin/frontend/fashionette/default/images/list-style-arrow.png"),
            http("request_68")
			.get("/skin/frontend/fashionette/default/images/gallery-navigation-arrows.png"),
            http("request_69")
			.get("/skin/frontend/fashionette/default/images/social-pinterest.jpeg?1484039661"),
            http("request_70")
			.get("/skin/frontend/fashionette/default/images/social-facebook.gif?1484039661"),
            http("request_71")
			.get(uri2 + "/j.php?a=210073&u=http%3A%2F%2Fwww.fashionette.de%2Fgucci-lion-leather-crossbody-bag-black&r=0.3912049066491693")
			.headers(headers_1),
            http("request_72")
			.get("/skin/frontend/fashionette/default/images/social-instagram.png?1484039661"),
            http("request_73")
			.get("/media/catalog/product/cache/1/image/1200x1200/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76114_7.jpg"),
            http("request_74")
			.get(uri2 + "/v.gif?a=210073&d=fashionette.de&u=E25948ABD0556B756EDCC911F65EE0E0&h=d21367168b97c0abe53b178547b83739&t=false&r=0.5793025931885087"),
            http("request_75")
			.get("/fonts/lato/Lato-Lig-webfont.woff")
			.headers(headers_75),
            http("request_76")
			.get("/media/catalog/product/cache/1/image/1200x1200/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76114_2.jpg"),
            http("request_77")
			.get("/skin/frontend/base/default/css/starplugins/ajax-loader.gif"),
            http("request_78")
			.get("/media/catalog/product/cache/1/image/1200x1200/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76114_3.jpg"),
            http("request_79")
			.get("/media/wysiwyg/paypal-sofort.gif"),
            http("request_80")
			.get("/media/catalog/product/cache/1/image/1200x1200/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76114_1.jpg"),
            http("request_81")
			.get("/skin/frontend/fashionette/default/images/creditcards.jpg"),
            http("request_82")
			.get("/media/catalog/product/cache/1/image/1200x1200/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76114_4.jpg"),
            http("request_83")
			.get("/media/catalog/product/cache/1/image/1200x1200/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76114_6.jpg"),
            http("request_84")
			.get("/media/catalog/product/cache/1/image/1200x1200/9df78eab33525d08d6e5fb8d27136e95/g/u/gucci-76114_5.jpg")))

	setUp(scn.inject(constantUsersPerSec(2) during(10 minutes))).protocols(httpProtocol)
}